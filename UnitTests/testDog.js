describe("Dog", function() {

    it("Dog name is Aban", function() {
        var dog = new Dog('Aban');
        assert.equal(dog.getName(), 'Aban');
    });

    it("Noname dog", function() {
        var dog = new Dog();
        assert.equal(dog.getName(), null);
    });

    it("Dog is barking", function() {
        var dog = new Dog('Aban');
        assert.equal(dog.bark(), 'Dog Aban is barking');
    });

});
