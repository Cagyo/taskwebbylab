function Dog(name){
    Animal.apply(this, arguments);
}

Dog.prototype = Object.create(Animal.prototype);
Dog.prototype.bark = function(){
    return 'Dog '+this.name+' is barking';
}

